A demo project MERN stack.
A social network for developers.

Installation:
1 - In the root of the project (Nodejs Back-end), do npm i;
2 - In the client project (React Fornt-End), do npm i;
3- Go back to the root of the project and do npm run dev to launch in a single command the Back-End server and the Front React (npm run dev launches synchronously npm run server and npm start)
